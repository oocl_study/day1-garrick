O:what did we learn today?

In the morning we learn the arrangements for the next four weeks and then we take an ice breaking to know classmates better. Then the teacher played us a video about creating a colledge of free learning. In the afternoon, we learn what concept map is and how to draw them. Our team discussed a topic on computers and draw the map on the whiteboard. Finally, we lean what the standup meeting is and we will take the standup meeting daily.



R:Please use one word to express your feelings about today's class.

useful.



I:What do you think about this? What was the most meaningful aspect of this activity?

Today we introduced some basics in the project development process, such as standup meetings and concept map, which are very meaningful.



D:Where do you most want to apply what you have learned today? What changes will you make?

I want to apply the concept map learned today to subsequent work, which can effectively organize my thoughts.